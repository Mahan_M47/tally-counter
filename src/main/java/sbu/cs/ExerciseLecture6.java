package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr)
    {
        long sum = 0;

        for (int i = 0; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr)
    {
        int[] reversed_arr = new int[arr.length];
        int n = arr.length - 1;

        for (int i = 0; i <= n; i++) {
            reversed_arr[n - i] = arr[i];
        }

        return reversed_arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        int row1, row2, col1, col2;
        row1 = m1.length;
        row2 = m2.length;
        col1 = m1[0].length;
        col2 = m2[0].length;

        if (col1 != row2) {
            throw new IllegalValueException();
        }

        double[][] ans = new double[row1][col2];

        for (int i = 0; i < row1; i++) {
            for (int j = 0; j < col2; j++) {
                for (int k = 0; k < col1; k++)
                {
                    ans[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }

        return ans;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> list_of_lists = new ArrayList<>();

        //List is an interface while ArrayList is an implementation of that interface.

        for (int i = 0; i < names.length; i++)
        {
            List<String> singleList = new ArrayList<>();

            for (int j = 0; j < names[0].length; j++) {
                singleList.add(names[i][j]);
            }

            list_of_lists.add(singleList);
        }

        return list_of_lists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n)
    {
        List<Integer> primes = new ArrayList<>();

        if (n % 2 == 0) {
            primes.add(2);
        }

        for (int i = 3; i <= n; i += 2) {
            if (isPrime(i) && n % i == 0)
            {
                primes.add(i);
            }
        }

        return primes;
    }

    private boolean isPrime(int n)
    {
        boolean flag = true;

        for (int i = 3; i <= Math.sqrt(n); i += 2) 
        {
            if (n % i == 0) {
                flag = false;
                break;
            }
        }
        
        return flag;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        List<String> words;

        words = Arrays.asList(line.split("\\W+"));

        return words;
    }
}
