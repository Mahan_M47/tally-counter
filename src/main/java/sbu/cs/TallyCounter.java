package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private int value;

    @Override
    public void count() {
        value++;
        if (value > 9999) {
            value = 9999;
        }
    }

    @Override
    public int getValue()
    {
        return value;
    }

    @Override
    public void setValue(int value1) throws IllegalValueException
    {
        if (value1 < 0 || value1 > 9999) {
            throw new IllegalValueException();
        }
        value = value1;
    }
}
