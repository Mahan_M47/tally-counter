package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n)
    {
        long ans = 1;
        for (int i = 2; i <= n; i++) {
            ans *= i;
        }
        return ans;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n)
    {
        long temp1, temp2 = 1, ans = 1;

        for (int i = 2; i < n; i++) {
            temp1 = temp2;
            temp2 = ans;
            ans = temp1 + temp2;
        }
        return ans;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)
    {
        StringBuilder reversed_word = new StringBuilder();

        reversed_word.append(word);
        reversed_word.reverse();

        return reversed_word.toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line)
    {
        boolean flag = false;
        line = line.replaceAll("\\s", "");
        line = line.toLowerCase();

        if (line.equals( reverse(line) )) {
            flag = true;
        }

        return flag;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2)
    {
        int rowLength = str1.length();
        int colLength = str2.length();

        char[][] plot = new char[rowLength][colLength];

        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++)
            {
                if ( str1.charAt(row) == str2.charAt(col) ) {
                    plot[row][col] = '*';
                }
                else plot[row][col] = ' ';

            }
        }

        return plot;
    }
}
