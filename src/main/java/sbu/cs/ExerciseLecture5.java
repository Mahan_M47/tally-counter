package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        Random rand = new Random();
        StringBuilder pass = new StringBuilder();

        for (int i = 0; i < length; i++) {
            char ch = (char) (rand.nextInt(26) + 97);
            pass.append(ch);
        }

        return pass.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception
    {
        if (length < 3) {
            throw new IllegalValueException();
        }

        Random rand = new Random();
        StringBuilder pass = new StringBuilder();

        char ch = (char) (rand.nextInt(10) + 48);
        pass.append(ch);

        ch = (char) (rand.nextInt(15) + 33);
        pass.append(ch);

        for (int i = 0; i < length - 2; i++) {
            ch = (char) (rand.nextInt(26) + 97);
            pass.append(ch);
        }

        return pass.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n)
    {
        boolean flag = false;
        ExerciseLecture4 num = new ExerciseLecture4();

        for (int i = 1; num.fibonacci(i) < n; i++)
        {
            long x = num.fibonacci(i);

            if ( n == x + ones_in_binary_format(x) ) {
                flag = true;
                break;
            }
        }

        return flag;
    }

    private int ones_in_binary_format (long n)
    {
        int ans = 0;

        while (n > 0) {
            ans += n % 2;
            n /= 2;
        }

        return ans;
    }
}
